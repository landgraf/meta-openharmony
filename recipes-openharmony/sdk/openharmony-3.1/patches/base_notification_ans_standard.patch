# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

Patch for //base/notification/ans_standard git repository of OpenHarmony 3.1 codebase.

Use NAPI_*_BOOL() macros to fix implicit cast of nullptr to boolean value
not supported by Clang 14.

Signed-off-by: Esben Haabendal <esben.haabendal@huawei.com>
Upstream-Status: Inappropriate [configuration/integration]

diff --git a/base/notification/ans_standard/interfaces/kits/napi/ans/src/publish.cpp b/base/notification/ans_standard/interfaces/kits/napi/ans/src/publish.cpp
index b0e575b..dfe8fa8 100644
--- a/base/notification/ans_standard/interfaces/kits/napi/ans/src/publish.cpp
+++ b/base/notification/ans_standard/interfaces/kits/napi/ans/src/publish.cpp
@@ -173,7 +173,7 @@ bool CheckProperty(const napi_env &env, const napi_value &content, const std::st
 
     bool hasProperty = false;
 
-    NAPI_CALL(env, napi_has_named_property(env, content, property.data(), &hasProperty));
+    NAPI_CALL_BOOL(env, napi_has_named_property(env, content, property.data(), &hasProperty));
     if (!hasProperty) {
         ANS_LOGW("Property %{public}s expected.", property.c_str());
     }
diff --git a/base/notification/ans_standard/interfaces/kits/napi/ans/src/reminder/reminder_common.cpp b/base/notification/ans_standard/interfaces/kits/napi/ans/src/reminder/reminder_common.cpp
index a3ebc189d762..490e8180ed06 100644
--- a/base/notification/ans_standard/interfaces/kits/napi/ans/src/reminder/reminder_common.cpp
+++ b/base/notification/ans_standard/interfaces/kits/napi/ans/src/reminder/reminder_common.cpp
@@ -63,7 +63,7 @@ bool ReminderCommon::GenActionButtons(
     for (size_t i = 0; i < length; i++) {
         napi_value actionButton = nullptr;
         napi_get_element(env, actionButtons, i, &actionButton);
-        NAPI_CALL(env, napi_typeof(env, actionButton, &valuetype));
+        NAPI_CALL_BOOL(env, napi_typeof(env, actionButton, &valuetype));
         if (valuetype != napi_object) {
             ANSR_LOGW("Wrong element type:%{public}s. object expected.", ACTION_BUTTON);
             return false;
@@ -259,15 +259,15 @@ bool ReminderCommon::GetStringUtf8(const napi_env &env, const napi_value &value,
     napi_valuetype valuetype = napi_undefined;
     size_t strLen = 0;
 
-    NAPI_CALL(env, napi_has_named_property(env, value, propertyName, &hasProperty));
+    NAPI_CALL_BOOL(env, napi_has_named_property(env, value, propertyName, &hasProperty));
     if (hasProperty) {
         napi_get_named_property(env, value, propertyName, &result);
-        NAPI_CALL(env, napi_typeof(env, result, &valuetype));
+        NAPI_CALL_BOOL(env, napi_typeof(env, result, &valuetype));
         if (valuetype != napi_string) {
             ANSR_LOGW("Wrong argument type:%{public}s. string expected.", propertyName);
             return false;
         }
-        NAPI_CALL(env, napi_get_value_string_utf8(env, result, propertyVal, size - 1, &strLen));
+        NAPI_CALL_BOOL(env, napi_get_value_string_utf8(env, result, propertyVal, size - 1, &strLen));
     }
     return hasProperty;
 }
@@ -305,13 +305,13 @@ bool ReminderCommon::GetPropertyValIfExist(const napi_env &env, const napi_value
         propertyVal = value;
     } else {
         bool hasProperty = false;
-        NAPI_CALL(env, napi_has_named_property(env, value, propertyName, &hasProperty));
+        NAPI_CALL_BOOL(env, napi_has_named_property(env, value, propertyName, &hasProperty));
         if (!hasProperty) {
             return false;
         }
         napi_get_named_property(env, value, propertyName, &propertyVal);
     }
-    NAPI_CALL(env, napi_typeof(env, propertyVal, &valuetype));
+    NAPI_CALL_BOOL(env, napi_typeof(env, propertyVal, &valuetype));
     if (valuetype != napi_number) {
         if (propertyName == nullptr) {
             ANSR_LOGW("Wrong argument type. number expected.");
@@ -329,12 +329,12 @@ bool ReminderCommon::GetObject(const napi_env &env, const napi_value &value,
     bool hasProperty = false;
     napi_valuetype valuetype = napi_undefined;
 
-    NAPI_CALL(env, napi_has_named_property(env, value, propertyName, &hasProperty));
+    NAPI_CALL_BOOL(env, napi_has_named_property(env, value, propertyName, &hasProperty));
     if (!hasProperty) {
         return false;
     }
     napi_get_named_property(env, value, propertyName, &propertyVal);
-    NAPI_CALL(env, napi_typeof(env, propertyVal, &valuetype));
+    NAPI_CALL_BOOL(env, napi_typeof(env, propertyVal, &valuetype));
     if (valuetype != napi_object) {
         ANSR_LOGW("Wrong argument type:%{public}s. object expected.", propertyName);
         return false;
